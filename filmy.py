import media
import fresh_tomatoes

star_wars = media.Movie("Star Wars","Wojny w galaktyce, plany gwiazdy smierci","http://cdn.movieweb.com/img.site/PHqgOEPBkHRkuv_1_l.jpg","https://www.youtube.com/watch?v=Ze2kpOZx_kU") 
                         

iron_man = media.Movie("Iron man", "Multimiliarder postanawia walczyć ze złem jako Iron Man.", "http://www.sideshowtoy.com/wp-content/uploads/2015/12/marvel-iron-man-mark-xlvi-sixth-scale-captain-america-civil-war-hot-toys-thumb-902622.jpg", "https://www.youtube.com/watch?v=jNo3zmhXE9Y")

ojciec_chrzestny = media.Movie("Ojciec Chrzestny", "Opowieść o nowojorskiej rodzinie mafijnej.","http://1.fwcdn.pl/po/10/89/1089/7196615.3.jpg","https://www.youtube.com/watch?v=sY1S34973zA")

gra_o_tron = media.Series("Gra o Tron", "Historia o zabijaniu glownych bohaterow", "http://1.fwcdn.pl/po/68/48/476848/7728238.3.jpg", "https://www.youtube.com/watch?v=fUJuOSD3Tb4")

himym = media.Series("Jak poznalem wasza matke", "Historia o tym jak pozanalem wasza matke", "http://1.fwcdn.pl/po/18/33/221833/7574923.3.jpg", "https://www.youtube.com/watch?v=yOe4_kdqsmU")

flash = media.Series("The Flash", "Historia o super bohaterze", "http://cdn1-www.superherohype.com/assets/uploads/gallery/the-flash-season-two/flashexcposter_5609ee26dff446-07323957.jpg", "https://www.youtube.com/watch?v=Yj0l7iGKh8g")

movies = [star_wars, iron_man, ojciec_chrzestny, gra_o_tron, himym, flash]

fresh_tomatoes.open_movies_page(movies)
